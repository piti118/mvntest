package com.maonoi;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public static int addStuff(int a, int b) {
        return a + b;
    }
}
